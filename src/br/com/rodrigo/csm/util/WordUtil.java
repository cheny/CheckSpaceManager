package br.com.rodrigo.csm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlToken;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTInline;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;

import br.com.rodrigo.csm.model.Ambiente;
import br.com.rodrigo.csm.model.ImagemIndicador;
import br.com.rodrigo.csm.model.Indicador;
import br.com.rodrigo.csm.model.Servicos;
import br.com.rodrigo.csm.model.Vistoria;


public class WordUtil {
	/*private static final String WORD_DIR = "c:\\checkspace";
	private static final String IMAGES_DIR = "c:\\checkspace\\images";
	private static final String EQUIPS_DIR = "c:\\checkspace\\images\\equipamentos";*/
	
	/*private static final String WORD_DIR = "d:\\home\\site\\wwwroot\\webapps\\word";
	private static final String IMAGES_DIR = "d:\\home\\site\\wwwroot\\webapps\\images";
	private static final String EQUIPS_DIR = "d:\\home\\site\\wwwroot\\webapps\\images\\equipamentos";*/
	
	/*private static final String WORD_DIR = "/Users/rodrigosordi/workspace/CheckSpaceManager/word";
	private static final String IMAGES_DIR = "/Users/rodrigosordi/workspace/CheckSpaceManager/images";
	private static final String EQUIPS_DIR = "/Users/rodrigosordi/workspace/CheckSpaceManager/images/equipamentos";*/
	
	private static final String WORD_DIR = "/home/checkspace/word";
	private static final String IMAGES_DIR = "/home/checkspace/images";
	private static final String EQUIPS_DIR = "/home/checkspace/images/equipamentos";
	
	public static int APROVADO = 1;
	public static int REPROVADO = 2;
	public static int NAO_APROVADO = 3;
	
	private static List<Indicador> revestimentoPisos;
	private static List<Indicador> revestimentoParedes;
	private static List<Indicador> soleiras;
	private static List<Indicador> peitoris;
	private static List<Indicador> tentosBaguetes;
	private static List<Indicador> esquadrias;
	private static List<Indicador> pisosMadeiras;
	private static List<Indicador> pinturasTetos;
	private static List<Indicador> pinturasParedes;
	private static List<Indicador> funcionaisTetos;
	private static List<Indicador> funcionaisParedes;
	private static List<Indicador> eletricas;
	private static List<Indicador> hidraulicas;
	
	public static File getWord(Vistoria vistoria, 
			List<Ambiente> ambientes, 
			List<Indicador> indicadores, 
			List<ImagemIndicador> imagemIndicadores) {
		//Create word folder
		File dir = new File (WORD_DIR);
		if (!dir.exists())
			dir.mkdir();
		//Create images folder
		dir = new File (IMAGES_DIR);
		if (!dir.exists()) 
			dir.mkdir();

		File file = new File(WORD_DIR + File.separator + "tmp.docx");
		
		ordenarIndicadores(indicadores);

		try {
			XWPFDocument document = new XWPFDocument();
			// Write the Document in file system
			FileOutputStream out = new FileOutputStream(file);
			
			
			// Capa
			//InputStream is = new FileInputStream(new File("C:\\Users\\Rodrigo\\workspaces\\workspace-JEE\\CheckSpaceManager\\WebContent\\life-up.jpg"));
			InputStream is = null;
			if (vistoria.getImagemPredioUrl() != null && !vistoria.getImagemPredioUrl().equals("")) {
				/*URL url = new URL(vistoria.getImagemPredioUrl());
				is = url.openStream();*/
				File image = new File(IMAGES_DIR + File.separator + vistoria.getImagemPredioUrl());
				if (image.exists())
					is = new FileInputStream(image);
			}
			
			createCapa (document, vistoria, is);

			//Introducao
			createIntroducao (document);
			
			// Item 1 - DADOS DO IMÓVEL
			createDadosImovel (document, vistoria);

			// Item 2 - MAPEAMENTO DO IMÓVEL
			createMapeamentoImovel (document, ambientes);
			
			// Item 3 - VALOR DE CONTRATO
			createValorContrato  (document, vistoria);

			// Item 4 - EQUIPAMENTOS UTILIZADOS
			createEquipamentosUtilizados (document);
			
			// Item 5 - DADOS DA VISTORIA
			XWPFParagraph paragraph = document.createParagraph();
			XWPFRun run = paragraph.createRun();
			run.setBold(true);
			run.setUnderline(UnderlinePatterns.SINGLE);
			run.setText("DADOS DA VISTORIA");
			
			// Item 5.1 - Dimensões
			createDimensoes (document, ambientes);
			
			// Item 5.2 - Conforto Térmico e Acústico
			createConfortoTermoAcustico (document, ambientes);

			
			//REVESTIMENTO_PISO
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Revestimento Piso.");
			createTable (document, revestimentoPisos);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.REVESTIMENTO_PISO);
			
			
			//REVESTIMENTO_PAREDE
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Revestimento Parede.");
			createTable (document, revestimentoParedes);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.REVESTIMENTO_PAREDE);
			
			
			//SOLEIRAS
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Soleiras.");
			createTable (document, soleiras);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.SOLEIRAS);
			
			
			//PEITORIS
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Peitoris.");
			createTable (document, peitoris);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.PEITORIS);
			
			
			//TENTOS_BAGUETES
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Tentos Baguetes.");
			createTable (document, tentosBaguetes);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.TENTOS_BAGUETES);
			
			
			//ESQUADRIAS
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Esquadrias.");
			createTable (document, esquadrias);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.ESQUADRIAS);
			
			
			//PISO_MADEIRA
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Piso Madeira.");
			createTable (document, pisosMadeiras);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.PISO_MADEIRA);
			
			
			//PINTURA_TETO
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Pintura Teto.");
			createTable (document, pinturasTetos);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.PINTURA_TETO);
			
			
			//PINTURA_PAREDE
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Pintura Parede.");
			createTable (document, pinturasParedes);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.PINTURA_PAREDE);
			
			
			//FUNCIONAL_TETO
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Funcional Teto.");
			createTable (document, funcionaisTetos);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.FUNCIONAL_TETO);
			
			
			//FUNCIONAL_PAREDE
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Funcional Parede.");
			createTable (document, funcionaisParedes);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.FUNCIONAL_PAREDE);
			
			
			//ELETRICA
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Elétrica.");
			createTable (document, eletricas);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.ELETRICA);
			
			
			//HIDRAULICA
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Hidráulica.");
			createTable (document, hidraulicas);
			
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run.setBold(true);
			run.setText("Observação: ");
			run = paragraph.createRun();
			run.setText("");
			run.addBreak();
			
			createImages (document, paragraph, run, imagemIndicadores, Servicos.HIDRAULICA);
			
			

			// Item 6 - CONCLUSÃO
			createConclusao (document);
			
			//Escreve no documento
			document.write(out);
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, e.getMessage(), e.toString());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		return file;
	}
	
	private static void createCapa (XWPFDocument document, Vistoria vistoria, InputStream is) throws InvalidFormatException, FileNotFoundException {
		XWPFParagraph paragraph = document.createParagraph();
		paragraph.setAlignment(ParagraphAlignment.CENTER);
		
		XWPFRun run = paragraph.createRun();
		
		run.setBold(true);
		run.setFontSize(28);
		run.setFontFamily("Calibri");
		run.setText("RELATÓRIO DE VISTORIA");
		run.addBreak();
		run.addBreak();
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setFontSize(22);
		run.setFontFamily("Calibri");
		run.setText("Edifício " + vistoria.getEmpreendimento());
		run.addBreak();
		run.addBreak();
		
		//Imagem do Prédio
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 400, 600);
			run.addBreak();
			run.addBreak();
		}
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setFontSize(16);
		DateFormat df = new SimpleDateFormat("MMMMMMMMM - yyyy", new Locale("pt", "BR"));
		run.setText(df.format(vistoria.getDataCompra()));
		run.addBreak(BreakType.PAGE);
	}
	
	private static void createIntroducao (XWPFDocument document) {
		//Introducao
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		
		run.setText("INTRODUÇÃO");
		run.addBreak();
		
		run = paragraph.createRun();
		
		run.setText("Prezada Sra.");
		run.addBreak();
		run.setText("A Check Space é uma empresa especializada na prestação de serviços de vistorias e avaliações de imóveis, residenciais e comerciais, novos ou usados, conferindo ao cliente de forma imparcial o estado do imóvel, apresentando todas as informações de forma clara e objetiva.");
		run.addBreak();
		
		//Apresentacao
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setBold(true);
		
		run.setText("APRESENTAÇÃO");
		run.addBreak();
		
		run = paragraph.createRun();
		run.setText("O presente relatório irá mostraras seguintes informações do imóvel citadas abaixo:");
		run.addBreak();
		run.setText("1. Dados do imóvel;");
		run.addBreak();
		run.setText("2. Mapeamento do Imóvel;");
		run.addBreak();
		run.setText("3. Valor do contrato;");
		run.addBreak();
		run.setText("4. Equipamentos Utilizados:");
		run.addBreak();
		run.setText("5. Dados da Vistoria;");
		run.addBreak();
		run.setText("6. Conclusão.");
		run.addBreak(BreakType.PAGE);
	}
	
	private static void createDadosImovel (XWPFDocument document, Vistoria vistoria) {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		run.setUnderline(UnderlinePatterns.SINGLE);
		run.setText("1. DADOS DO IMÓVEL");
		run.addBreak();
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Proprietário (a): ");
		run = paragraph.createRun();
		run.setText(vistoria.getProprietario());
		run.addBreak();
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Localização: ");
		run = paragraph.createRun();
		run.setText(vistoria.getEndereco());
		run.addBreak();
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Tamanho de área útil líquida: ");
		run = paragraph.createRun();
		Double metrosQuadrados = vistoria.getMetrosQuadrados();
		if (metrosQuadrados != null) {
			run.setText(metrosQuadrados.toString().replace(".", ","));
		} else {
			run.setText("0");
		}
		run.addBreak();
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Data da Vistoria: ");
		run = paragraph.createRun();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		if (vistoria.getDataVistoria() != null) {
			run.setText(df.format(vistoria.getDataVistoria()));
		} else {
			run.setText("");
		}
		run.addBreak();
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Horário: ");
		run = paragraph.createRun();
		df = new SimpleDateFormat("HH:mm");
		if (vistoria.getHorarioAgendado() != null) {
			run.setText(df.format(vistoria.getHorarioAgendado()) + " horas.");
		} else {
			run.setText("");
		}
		run.addBreak();
		
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Condições Climáticas: ");
		run = paragraph.createRun();
		run.setText(vistoria.getClima());
		run.addBreak();
	}
	
	private static void createMapeamentoImovel (XWPFDocument document, List<Ambiente> ambientes) {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		run.setUnderline(UnderlinePatterns.SINGLE);
		run.setText("2. MAPEAMENTO DO IMÓVEL");
		run.addBreak();
		
		run = paragraph.createRun();
		for (Ambiente ambiente : ambientes) {
			run.setText("* " + ambiente.getNome().replace('_', ' '));
			run.addBreak();
		}
	}
	
	private static void createValorContrato (XWPFDocument document, Vistoria vistoria) {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		run.setUnderline(UnderlinePatterns.SINGLE);
		run.setText("3. VALOR DE CONTRATO");
		run.addBreak();
		
		run = paragraph.createRun();
		/*DecimalFormat formatter = new DecimalFormat("###,###,###.00");
		String moneyString = formatter.format(vistoria.getValor());
		moneyString = moneyString.replace(" ", ",");
		moneyString = moneyString.replace(",", ".");*/
		run.setText("R$ " + "0,00");
		run.addBreak();
	}
	
	private static void createEquipamentosUtilizados(XWPFDocument document) throws FileNotFoundException, InvalidFormatException {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		run.setUnderline(UnderlinePatterns.SINGLE);
		run.setText("4. EQUIPAMENTOS UTILIZADOS");
		run.addBreak();
		
		File dir = new File (EQUIPS_DIR);
		if (!dir.exists()) 
			dir.mkdir();
		
		//trena_layser
		InputStream is = null;
		File image = new File(EQUIPS_DIR + File.separator + "trena_layser.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 90, 205);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Trena a Layser");
			run.addBreak();
			run.addBreak();
		}
		
		//nivel_layser
		image = new File(EQUIPS_DIR + File.separator + "nivel_layser.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 165, 207);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Nivel a Layser");
			run.addBreak();
			run.addBreak();
		}
		
		//multimetro_digital
		image = new File(EQUIPS_DIR + File.separator + "multimetro_digital.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 165, 207);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Multimetro Digital");
			run.addBreak();
			run.addBreak();
		}
		
		//termometro_infravermelho
		image = new File(EQUIPS_DIR + File.separator + "termometro_infravermelho.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 103, 200);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Termometro Infravermelho");
			run.addBreak();
			run.addBreak();
		}
		
		//decibel_digital
		image = new File(EQUIPS_DIR + File.separator + "decibel_digital.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 126, 202);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Decibel Digital");
			run.addBreak();
			run.addBreak();
		}
		
		//ventosa_simples
		image = new File(EQUIPS_DIR + File.separator + "ventosa_simples.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 200, 215);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Ventosa Simples");
			run.addBreak();
			run.addBreak();
		}
		
		//ventosa_dupla
		image = new File(EQUIPS_DIR + File.separator + "ventosa_dupla.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 217, 141);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Ventosa Dupla");
			run.addBreak();
			run.addBreak();
		}
		
		//martelo_madeira
		image = new File(EQUIPS_DIR + File.separator + "martelo_madeira.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 216, 211);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Martelo Madeira");
			run.addBreak();
			run.addBreak();
		}
		
		//trena_5m
		image = new File(EQUIPS_DIR + File.separator + "trena_5m.png");
		if (image.exists())
			is = new FileInputStream(image);
		
		if (is != null) {
			String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
			paragraph = document.createParagraph();
			run = paragraph.createRun();
			run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 212, 213);
			run.addBreak();
			
			run = paragraph.createRun();
			run.setText("Trena 5m");
			run.addBreak();
			run.addBreak();
		}
		
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setText("Toda a vistoria é realizada baseada em Normas Brasileiras Regulamentadoras, as quais padronizam a melhor técnica de medição e aplicação dos equipamentos acima.");
		run.addBreak();

		// Item 4.1 - Legenda
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Legenda");
		run.addBreak();
		run.setText("A - Aprovado");
		run.addBreak();
		run.setText("R - Reprovado");
		run.addBreak();
		run.setText("NA - Não Aplicável");
		run.addBreak(BreakType.PAGE);
		
	}
	
	private static void createDimensoes (XWPFDocument document, List<Ambiente> ambientes) throws Exception {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		run.setText("5.1. Dimensões");
		
		//Tabela de Dimensoes
		XWPFTable table = document.createTable();
		XWPFTableRow row = table.getRow(0);
		table.setCellMargins(1, 1, 100, 30);
		
		XWPFTableCell cell = row.getCell(0);
		cell.setText("Item");
		CTTcPr tcpr = cell.getCTTc().addNewTcPr();
		tcpr.addNewTcW().setW(BigInteger.valueOf(600));
		
		cell = row.addNewTableCell();
		cell.setText("Ambientes");
		cell.getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));
		
		cell = row.addNewTableCell();
		cell.setText("Área (m²)");
		cell.getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));
		
		cell = row.addNewTableCell();
		cell.setText("Pé Direito (m)");
		cell.getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));
		
		if (ambientes != null && ambientes.size() > 0) {
			Double total = 0.0d;
			int i = 1;
			for (Ambiente ambiente : ambientes) {
				if (ambiente.getArea() != null)
					total += ambiente.getArea();
				
				row = table.createRow();
				
				String id = String.valueOf(i);
				String nome_ambiente = ambiente.getNome() != null ? ambiente.getNome() : "";
				String area = ambiente.getArea() != null ? ambiente.getArea().toString().replace('.', ',') : "";
				String peDireito = ambiente.getPeDireito() != null ? ambiente.getPeDireito().toString().replace('.', ',') : "";
				
				row.getCell(0).setText(id);
				row.getCell(1).setText(nome_ambiente);
				row.getCell(2).setText(area);
				row.getCell(3).setText(peDireito);
				
				i++;
			}
			
			//Total
			row = table.createRow();
			row.getCell(1).setText("ÁREA TOTAL (m²)");
			row.getCell(2).setText(total.toString().replace('.', ',') + "m²");
			
			
		} else {
			row = table.createRow();
			row.getCell(0).setText("");
			row.getCell(1).setText("");
			row.getCell(2).setText("");
			row.getCell(3).setText("");
		}
		
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Observação: ");
		run = paragraph.createRun();
		run.setText("A área mencionada acima não inclui paredes, pilares, shaft, poço de elevador, depósitos, vaga de garagem e/ou outras áreas externas à unidade tais como frações ideais.");
		run.addBreak();

		// Item 5.1.1
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("5.1.1. Croqui planta baixa-in loco.");
		run.addBreak(BreakType.PAGE);
	}
	
	private static void createConfortoTermoAcustico (XWPFDocument document, List<Ambiente> ambientes) throws Exception {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		run.setText("5.2. Conforto Térmico e Acústico.");
		
		//Tabela de Dimensoes
		XWPFTable table = document.createTable();
		
		XWPFTableRow row = table.getRow(0);
		row.getCell(0).setText("Item");
		row.addNewTableCell().setText("Ambientes");
		row.addNewTableCell().setText("Acustico (dBA)");
		row.addNewTableCell().setText("");
		row.addNewTableCell().setText("Térmico (ºC)");
		row.addNewTableCell().setText("");
		//row.addNewTableCell().setText("Observações");
		
		row = table.createRow();
		row.getCell(0).setText("");
		row.getCell(1).setText("");
		row.getCell(2).setText("Aberto");
		row.getCell(3).setText("Fechado");
		row.getCell(4).setText("Aberto");
		row.getCell(5).setText("Fechado");
		//row.getCell(6).setText("");
		
		if (ambientes != null && ambientes.size() > 0) {
			int i = 1;
			for (Ambiente ambiente : ambientes) {
				row = table.createRow();
				
				String id = String.valueOf(i);
				String nome_ambiente = ambiente.getNome();
				
				String acusticoAberto = ambiente.getAcusticoAberto() != null ? ambiente.getAcusticoAberto().toString().replace('.', ',') : "";
				String acusticoFechado = ambiente.getAcusticoFechado() != null ? ambiente.getAcusticoFechado().toString().replace('.', ',') : "";
				String termoAberto = ambiente.getTermoAberto() != null ? ambiente.getTermoAberto().toString().replace('.', ',') : "";
				String termoFechado = ambiente.getTermoFechado() != null ? ambiente.getTermoFechado().toString().replace('.', ',') : "";
				
				//String obs = ambiente.getObservacao();
				
				row.getCell(0).setText(id);
				row.getCell(1).setText(nome_ambiente);
				row.getCell(2).setText(acusticoAberto);
				row.getCell(3).setText(acusticoFechado);
				row.getCell(4).setText(termoAberto);
				row.getCell(5).setText(termoFechado);
				//row.getCell(6).setText(obs);
				
				i++;
			}
		} else {
			row = table.createRow();
			row.getCell(0).setText("");
			row.getCell(1).setText("");
			row.getCell(2).setText("");
			row.getCell(3).setText("");
			row.getCell(4).setText("");
			row.getCell(5).setText("");
			//row.getCell(6).setText("");
		}
		
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setBold(true);
		run.setText("Observação: ");
		run = paragraph.createRun();
		run.setText("A avaliação foi realizada de acordo com a ABNTNBR 10.152- Norma de ruído para conforto acústico e ABNT NBR 10.151 - Avaliação do ruído em áreas habitadas, visando o conforto da comunidade - Procedimento.");
		run.addBreak(BreakType.PAGE);
	}
	
	
	
	private static void createConclusao (XWPFDocument document) {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setBold(true);
		run.setUnderline(UnderlinePatterns.SINGLE);
		run.setText("CONCLUSÃO");
		run.addBreak();
		
		run = paragraph.createRun();
		run.setText("A partir da vistoria realizada, foram encontrados itens com falha de execução. Não foi possível verificar as instalações elétricas devido à falta de energia na unidade, podendo haver vícios ocultos nos quadros de distribuição, soquetes, tomada, interruptores, etc. Não conseguimos avaliar vazamentos no apartamento do andar inferior, pois o acesso não foi permitido, aconselhável que antes do recebimento da unidade este item se já verificado. Estamos à disposição até 10 (dez) dias após o recebimento deste para dúvidas.");
		run.addBreak();
		
		//Dados Finais
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setText("Check Space");
		run.addBreak();
		
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setText("Mariana Cury Sobreira");
		run.addBreak();
		run.setText("marianacury@checkspace.com.br");
		run.addBreak();
		
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setText("Kamila Regina de Souza Reis");
		run.addBreak();
		run.setText("kamilareis@checkspace.com.br");
		run.addBreak();
		
		paragraph = document.createParagraph();
		run = paragraph.createRun();
		run.setText("Tel: (11) 4324-9250 / 4324-9251");
		run.addBreak();
	}
	
	
	
	private static void createTable (XWPFDocument document, List<Indicador> indicadores) {
		//Tabela de Dimensoes
		XWPFTable table = document.createTable();
		XWPFTableRow row = table.getRow(0);
		
		XWPFTableCell cell = row.getCell(0);
		cell.setText("Item");
		CTTcPr tcpr = cell.getCTTc().addNewTcPr();
		tcpr.addNewTcW().setW(BigInteger.valueOf(600));
		
		cell = row.addNewTableCell();
		cell.setText("Ambientes");
		tcpr = cell.getCTTc().addNewTcPr();
		tcpr.addNewTcW().setW(BigInteger.valueOf(2500));
		
		cell = row.addNewTableCell();
		cell.setText("A");
		tcpr = cell.getCTTc().addNewTcPr();
		tcpr.addNewTcW().setW(BigInteger.valueOf(400));
		
		cell = row.addNewTableCell();
		cell.setText("R");
		tcpr = cell.getCTTc().addNewTcPr();
		tcpr.addNewTcW().setW(BigInteger.valueOf(400));
		
		cell = row.addNewTableCell();
		cell.setText("NA");
		tcpr = cell.getCTTc().addNewTcPr();
		tcpr.addNewTcW().setW(BigInteger.valueOf(400));
		
		cell = row.addNewTableCell();
		cell.setText("Observações");
		tcpr = cell.getCTTc().addNewTcPr();
		tcpr.addNewTcW().setW(BigInteger.valueOf(10000));
		
		if (indicadores != null && indicadores.size() > 0) {
			//Separa todos indicadores com o mesmo ambiente
			Map<String, Indicador> filtered = new HashMap<String, Indicador> ();
			for (Indicador indicador : indicadores) {
				//System.out.println(indicador.getId() + " " + indicador.getServico().getNome());
				if (indicador.getServico() != null && indicador.getServico().getAmbiente() != null && indicador.getServico().getAmbiente().getNome() != null) {
					String key = indicador.getServico().getAmbiente().getNome();
					if (filtered.containsKey(key)) {
						Indicador tmp = filtered.get(key);
						
						//Atualiza status
						if (tmp.getIndicador() != REPROVADO) {
							if (indicador.getIndicador() == REPROVADO)
								tmp.setIndicador(REPROVADO);
							else if (indicador.getIndicador() == APROVADO)
								tmp.setIndicador(APROVADO);
						} else if (tmp.getIndicador() != APROVADO) {
							if (indicador.getIndicador() == APROVADO)
								tmp.setIndicador(APROVADO);
						}
						
						tmp.setComentario(tmp.getComentario() + ", " + indicador.getComentario());
						filtered.put(key, tmp);
					} else {
						filtered.put(key, indicador);
					}
				}
			}
			
		
		
			int i = 1;
			for (Indicador indicador : filtered.values()) {
				row = table.createRow();
				
				String item = String.valueOf(i);
				String ambientes = indicador.getServico() != null && 
											indicador.getServico().getAmbiente() != null ? 
											indicador.getServico().getAmbiente().getNome() : "";
				String a = null;
				String r = null;
				String na = null;
				int value = indicador.getIndicador();
				if (value == 1) {
					a = "X";
				} else if (value == 2) {
					r = "X";
				} else if (value == 3) {
					na = "X";
				}
				String obs = indicador.getComentario();
				
				row.getCell(0).setText(item);
				row.getCell(1).setText(ambientes);
				row.getCell(2).setText(a);
				row.getCell(3).setText(r);
				row.getCell(4).setText(na);
				row.getCell(5).setText(obs);
				i++;
			}
		} else {
			row = table.createRow();
			row.getCell(0).setText("");
			row.getCell(1).setText("");
			row.getCell(2).setText("");
			row.getCell(3).setText("");
			row.getCell(4).setText("");
			row.getCell(5).setText("");
		}
	}
	
	private static void createImages (XWPFDocument document, XWPFParagraph paragraph, XWPFRun run, List<ImagemIndicador> imagemIndicadores, String servico) {
		//Imagens
		for (ImagemIndicador imagemIndicador : imagemIndicadores) {
			if (imagemIndicador.getIndicador() == null || 
					imagemIndicador.getIndicador().getServico() == null || 
					!imagemIndicador.getIndicador().getServico().getNome().equals(servico))
				continue;
			
			if (imagemIndicador.getImagemUrl() != null) {
				File image = new File(IMAGES_DIR + File.separator + imagemIndicador.getImagemUrl());
				if (image.exists()) {
					try {
						InputStream is = new FileInputStream(image);
						
						if (is != null) {
							String id = document.addPictureData(is, Document.PICTURE_TYPE_JPEG);
							run = createPicture(paragraph, id,document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG), 400, 400);
							run.addBreak();
						}
						run.setText(imagemIndicador.getComentario());
						run.addBreak();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		run.addBreak(BreakType.PAGE);
	}
	
	private static XWPFRun createPicture (XWPFParagraph paragraph, String blipId, int id, int width, int height) {
		 final int EMU = 9525;
	        width *= EMU;
	        height *= EMU;
	        //String blipId = getAllPictures().get(id).getPackageRelationship().getId();
	        paragraph.setAlignment(ParagraphAlignment.CENTER);

	        XWPFRun run = paragraph.createRun();
	        CTInline inline = run.getCTR().addNewDrawing().addNewInline();

	        String picXml = "" +
	                "<a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">" +
	                "   <a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">" +
	                "      <pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">" +
	                "         <pic:nvPicPr>" +
	                "            <pic:cNvPr id=\"" + id + "\" name=\"Generated\"/>" +
	                "            <pic:cNvPicPr/>" +
	                "         </pic:nvPicPr>" +
	                "         <pic:blipFill>" +
	                "            <a:blip r:embed=\"" + blipId + "\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\"/>" +
	                "            <a:stretch>" +
	                "               <a:fillRect/>" +
	                "            </a:stretch>" +
	                "         </pic:blipFill>" +
	                "         <pic:spPr>" +
	                "            <a:xfrm>" +
	                "               <a:off x=\"0\" y=\"0\"/>" +
	                "               <a:ext cx=\"" + width + "\" cy=\"" + height + "\"/>" +
	                "            </a:xfrm>" +
	                "            <a:prstGeom prst=\"rect\">" +
	                "               <a:avLst/>" +
	                "            </a:prstGeom>" +
	                "         </pic:spPr>" +
	                "      </pic:pic>" +
	                "   </a:graphicData>" +
	                "</a:graphic>";

	        //CTGraphicalObjectData graphicData = inline.addNewGraphic().addNewGraphicData();
	        XmlToken xmlToken = null;
	        try
	        {
	            xmlToken = XmlToken.Factory.parse(picXml);
	        }
	        catch(XmlException xe)
	        {
	            xe.printStackTrace();
	        }
	        inline.set(xmlToken);
	        //graphicData.set(xmlToken);

	        inline.setDistT(0);
	        inline.setDistB(0);
	        inline.setDistL(0);
	        inline.setDistR(0);

	        CTPositiveSize2D extent = inline.addNewExtent();
	        extent.setCx(width);
	        extent.setCy(height);

	        CTNonVisualDrawingProps docPr = inline.addNewDocPr();
	        docPr.setId(id);
	        docPr.setName("Picture " + id);
	        docPr.setDescr("Generated");
	        
	        return run;
	}
	
	private static void ordenarIndicadores(List<Indicador> todosIndicadores) {
		revestimentoPisos = new ArrayList<Indicador>();
		revestimentoParedes = new ArrayList<Indicador>();
		soleiras = new ArrayList<Indicador>();
		peitoris = new ArrayList<Indicador>();
		tentosBaguetes = new ArrayList<Indicador>();
		esquadrias = new ArrayList<Indicador>();
		pisosMadeiras = new ArrayList<Indicador>();
		pinturasTetos = new ArrayList<Indicador>();
		pinturasParedes = new ArrayList<Indicador>();
		funcionaisTetos = new ArrayList<Indicador>();
		funcionaisParedes = new ArrayList<Indicador>();
		eletricas = new ArrayList<Indicador>();
		hidraulicas = new ArrayList<Indicador>();
		
		for (Indicador indicador : todosIndicadores) {
			if (indicador != null &&
					indicador.getServico() != null &&
					indicador.getServico().getNome() != null) {
				if (indicador.getServico().getNome().equals(Servicos.REVESTIMENTO_PISO)) {
					revestimentoPisos.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.REVESTIMENTO_PAREDE)) {
					revestimentoParedes.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.SOLEIRAS)) {
					soleiras.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.PEITORIS)) {
					peitoris.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.TENTOS_BAGUETES)) {
					tentosBaguetes.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.ESQUADRIAS)) {
					esquadrias.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.PISO_MADEIRA)) {
					pisosMadeiras.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.PINTURA_TETO)) {
					pinturasTetos.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.PINTURA_PAREDE)) {
					pinturasParedes.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.FUNCIONAL_TETO)) {
					funcionaisTetos.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.FUNCIONAL_PAREDE)) {
					funcionaisParedes.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.ELETRICA)) {
					eletricas.add(indicador);
				} else if (indicador.getServico().getNome().equals(Servicos.HIDRAULICA)) {
					hidraulicas.add(indicador);
				}
				
			}
		}
	}
}
