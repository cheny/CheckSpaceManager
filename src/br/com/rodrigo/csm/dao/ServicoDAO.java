package br.com.rodrigo.csm.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.rodrigo.csm.model.Servico;

public class ServicoDAO extends GenericDAOImpl<Servico, Integer> {
	@SuppressWarnings("unchecked")
	public List<Servico> findByVistoria (int vistoriaId) {
		List<Servico> result = null;
		
		String jpql = "select s from Servico s where s.ambiente.vistoria.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriaId);
		result = query.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Servico> findByAmbiente (int ambienteId) {
		List<Servico> result = null;
		
		String jpql = "select s from Servico s where s.ambiente.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", ambienteId);
		result = query.getResultList();
		
		return result;
	}
}
