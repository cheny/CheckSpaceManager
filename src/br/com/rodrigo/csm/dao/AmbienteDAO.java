package br.com.rodrigo.csm.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.rodrigo.csm.model.Ambiente;

public class AmbienteDAO extends GenericDAOImpl<Ambiente, Integer> {
	@SuppressWarnings("unchecked")
	public List<Ambiente> findByVistoria (int vistoriaId) {
		List<Ambiente> result = null;
		
		String jpql = "select a from Ambiente a where a.vistoria.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriaId);
		result = query.getResultList();
		
		return result;
	}
}
