package br.com.rodrigo.csm.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import br.com.rodrigo.csm.model.JsonBehaviour;

public class GenericDAOImpl<T extends JsonBehaviour, PK extends Serializable> implements GenericDAO<T, PK> {
	protected EntityManager manager;
	private Class<T> entityClass;
	
	@Override
	public void save(T entity) {
		try {
			manager.getTransaction().begin();
			manager.persist(entity);
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				manager.getTransaction().rollback();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			throw e;
		}
	}
	
	@Override
	public void saveList(List<T> list) {
		try {
			manager.getTransaction().begin();
			for (T entity : list)
				manager.persist(entity);
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				manager.getTransaction().rollback();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			throw e;
		}
	}
	
	@Override
	public void saveOrUpdateMap(Map<String, T> map) {
		try {
			manager.getTransaction().begin();
			for (T entity : map.values()) {
				if (entity != null) {
					if (entity.getId() == null || entity.getId().equals(0) || entity.getId().equals("")) {
						entity.setId(null);
						manager.persist(entity);
					} else
						manager.merge(entity);
				}
			}
			
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				manager.getTransaction().rollback();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			throw e;
		}
	}
	
	@Override
	public void update(T entity) {
		try {
			manager.getTransaction().begin();
			manager.merge(entity);
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				manager.getTransaction().rollback();
			} catch (Exception e2) {
				
			}
			throw e;
		}
	}
	
	@Override
	public void delete(T entity) {
		try{
			Object pk = null;//DataBaseManager.getFactory().getPersistenceUnitUtil().getIdentifier(entity);
			entity = (T) manager.find(getEntityClass(), pk);
			manager.remove(entity);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	@Override
	public T findById(PK pk) {
		T result = null;
		try {
			result = (T) manager.find(getEntityClass(), pk);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		List<T> result = null;
		try {
			String query = "FROM " + getEntityClass().getSimpleName();
			result = manager.createQuery(query).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	
	/*@Override
	public void close () {
		if (manager != null && manager.isOpen())
			manager.close();
	}*/
	
	@Override
	public void setEntityManager (EntityManager manager) {
		this.manager = manager;
	}
	
	@SuppressWarnings("unchecked")
	private Class<T> getEntityClass () {
		if (entityClass == null) {
			entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		}
		return entityClass;
	}
}
