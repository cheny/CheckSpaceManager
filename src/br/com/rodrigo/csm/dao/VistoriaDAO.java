package br.com.rodrigo.csm.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.rodrigo.csm.model.Vistoria;

public class VistoriaDAO extends GenericDAOImpl<Vistoria, Integer> {
	@SuppressWarnings("unchecked")
	public List<Vistoria> findByVistoriador (int vistoriadorId) {
		List<Vistoria> result = null;
		
		String jpql = "select v from Vistoria v where v.vistoriador.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriadorId);
		result = query.getResultList();
		
		return result;
	}
}
