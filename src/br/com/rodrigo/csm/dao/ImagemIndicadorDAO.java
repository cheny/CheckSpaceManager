package br.com.rodrigo.csm.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.rodrigo.csm.model.ImagemIndicador;

public class ImagemIndicadorDAO extends GenericDAOImpl<ImagemIndicador, Integer> {
	@SuppressWarnings("unchecked")
	public List<ImagemIndicador> findByVistoriador (int vistoriadorId) {
		List<ImagemIndicador> result = null;
		
		String jpql = "select ii from ImagemIndicador ii where ii.indicador.servico.ambiente.vistoria.vistoriador.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriadorId);
		result = query.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<ImagemIndicador> findByVistoria (int vistoriaId) {
		List<ImagemIndicador> result = null;
		
		String jpql = "select ii from ImagemIndicador ii where ii.indicador.servico.ambiente.vistoria.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriaId);
		result = query.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<ImagemIndicador> findByVistoriaAndAmbienteAndServico (int vistoriaId, String nome_ambiente, String nome_servico) {
		List<ImagemIndicador> result = null;
		
		String jpql = "select ii from ImagemIndicador ii where "
				+ "ii.indicador.servico.ambiente.vistoria.id = :vistoriaId and "
				+ "ii.indicador.servico.nome = :nome_servico and "
				+ "ii.indicador.servico.ambiente.nome = :nome_ambiente";
		Query query = manager.createQuery(jpql);
		query.setParameter("vistoriaId", vistoriaId);
		query.setParameter("nome_ambiente", nome_ambiente);
		query.setParameter("nome_servico", nome_servico);
		result = query.getResultList();
		
		return result;
	}
}
