package br.com.rodrigo.csm.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.rodrigo.csm.model.Indicador;

public class IndicadorDAO extends GenericDAOImpl<Indicador, Integer> {
	@SuppressWarnings("unchecked")
	public List<Indicador> findByVistoria (int vistoriaId) {
		List<Indicador> result = null;
		
		String jpql = "select i from Indicador i where i.servico.ambiente.vistoria.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriaId);
		result = query.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Indicador> findByVistoriador (int vistoriadorId) {
		List<Indicador> result = null;
		
		String jpql = "select i from Indicador i where i.servico.ambiente.vistoria.vistoriador.id = :id";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriadorId);
		result = query.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getNomeAmbientesByVistoria (int vistoriaId) {
		List<String> result = null;
		
		String jpql = "select i.servico.ambiente.nome from Indicador i where i.servico.ambiente.vistoria.id = :id group by i.servico.ambiente.nome";
		Query query = manager.createQuery(jpql);
		query.setParameter("id", vistoriaId);
		result = query.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Indicador> findByVistoriaAndAmbienteAndServico (int vistoriaId, String nome_ambiente, String nome_servico) {
		List<Indicador> result = null;
		
		String jpql = "select i from Indicador i where i.servico.ambiente.vistoria.id = :vistoriaId and i.servico.ambiente.nome = :nome_ambiente and i.servico.nome = :nome_servico";
		Query query = manager.createQuery(jpql);
		query.setParameter("vistoriaId", vistoriaId);
		query.setParameter("nome_ambiente", nome_ambiente);
		query.setParameter("nome_servico", nome_servico);
		result = query.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Indicador> findByVistoriaAndAmbienteAndServicoAndName (int vistoriaId, String nome_ambiente, String nome_servico, String nome_indicador) {
		List<Indicador> result = null;
		
		String jpql = "select i from Indicador i where i.servico.ambiente.vistoria.id = :vistoriaId and i.servico.ambiente.nome = :nome_ambiente and i.servico.nome = :nome_servico and i.nome = :nome_indicador";
		Query query = manager.createQuery(jpql);
		query.setParameter("vistoriaId", vistoriaId);
		query.setParameter("nome_ambiente", nome_ambiente);
		query.setParameter("nome_servico", nome_servico);
		query.setParameter("nome_indicador", nome_indicador);
		result = query.getResultList();
		
		return result;
	}
}
