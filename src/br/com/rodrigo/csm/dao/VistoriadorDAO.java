package br.com.rodrigo.csm.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.rodrigo.csm.model.Vistoriador;

public class VistoriadorDAO extends GenericDAOImpl<Vistoriador, Integer> {
	@SuppressWarnings("unchecked")
	public Vistoriador login (String username, String password) {
		Vistoriador result = null;
		
		List<Vistoriador> list = null;
		String jpql = "select v from Vistoriador v where v.usuario = :usuario and v.senha = :senha";
		Query query = manager.createQuery(jpql);
		query.setParameter("usuario", username);
		query.setParameter("senha", password);
		list = query.getResultList();
		
		if (list != null && list.size() > 0)
			result = list.get(0);
		
		return result;
	}
}
