package br.com.rodrigo.csm.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

public interface GenericDAO<T extends Serializable, PK extends Serializable> {
	public void save(T entity);
	
	public void saveList (List<T> list);
	
	public void saveOrUpdateMap (Map<String, T> map);
	
	public void update(T entity);
	
	public void delete(T entity);
	
	public T findById(PK pk);
	
	public List<T> findAll();
	
	public void setEntityManager(EntityManager manager);
	
	//public void close ();
}
