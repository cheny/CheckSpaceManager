package br.com.rodrigo.csm.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.rodrigo.csm.model.JsonBehaviour;
import br.com.rodrigo.csm.model.Vistoriador;
import br.com.rodrigo.csm.ws.Server;

public class WebServiceTests {
	public static void main(String[] args) {
		testSaveList();
	}
	
	public static void testSaveList() {
		Vistoriador vistoriador = new Vistoriador();
		vistoriador.setId(null);
		vistoriador.setNome("Vistoriador1");
		vistoriador.setSenha("1234");
		vistoriador.setUsuario("user");
		
		Vistoriador vistoriador2 = new Vistoriador();
		vistoriador2.setId(null);
		vistoriador2.setNome("Vistoriador2");
		vistoriador2.setSenha("1234");
		vistoriador2.setUsuario("user");
		
		List<Vistoriador> vistoriadores = new ArrayList<Vistoriador>();
		vistoriadores.add(vistoriador);
		vistoriadores.add(vistoriador2);
		
		Server server = new Server();
		String json = JsonBehaviour.getJsonFromList(vistoriadores);
		String result = server.saveVistoriadorList(json);
		System.out.println(result);
	}
	
	public static void testLoadAllVistoriadores () {
		Server server = new Server();
		String json = server.findAllVistoriador();
		System.out.println(json);
	}
	
	public static void testSaveImage () {
		File file = new File("/users/rodrigosordi/desktop/downloaded.jpg");
		String name_encoded = "teste;" + "";
		
		Server server = new Server();
		server.saveImage(name_encoded);
		
		System.out.println("Finish");
	}
	
	/*public static void testVistoria () {
		String json = "{\"proprietario\":\"proprietario\", \"empreendimento\":\"empreendimento\", \"valor\":\"1000.5\", \"endereco\":\"endereco\", \"unidade\":\"unidade\", \"bloco\":\"bloco\", \"dataCompra\":\"26/08/2015 11:52:28\", \"metragem\":\"1.5\", \"padrao\":\"padrao\", \"dataVistoria\":\"26/08/2015 11:52:28\", \"horarioAgendado\":\"26/08/2015 11:52:28\", \"inicioVistoria\":\"26/08/2015 11:52:28\", \"incorporadora\":\"incorporadora\", \"terminoVistoria\":\"26/08/2015 11:52:28\", \"construtora\":\"construtora\", \"clima\":\"clima\", \"vistoriador\":\"3\"}";
		Server server = new Server ();
		String result = server.saveOrUpdateVistoria (json);
		System.out.println(result);
	}
	
	public static void testGetImage () {
		Server server = new Server ();
		String result = server.getImage ("download.jpeg");
		System.out.println(result);
	}
	
	public static void testWebService () {
		Server ws = new Server();
		String json = ws.findVistoriaByVistoriador(1);
		System.out.println(json);
	}
	
	public static void testNames () {
		IndicadorWS ws = new IndicadorWS();
		String result = ws.getNomeAmbientesByVistoria(12);
		System.out.println(result);
	}
	
	public static void testVistoriaJsonSave () {
		String json = "{\"proprietario\":\"proprietario\", \"empreendimento\":\"empreendimento\", \"valor\":\"1000.5\", \"endereco\":\"endereco\", \"unidade\":\"unidade\", \"bloco\":\"bloco\", \"dataCompra\":\"11/08/2015 23:27:42\", \"metragem\":\"1.5\", \"padrao\":\"padrao\", \"dataVistoria\":\"11/08/2015 23:27:42\", \"horarioAgendado\":\"11/08/2015 23:27:42\", \"inicioVistoria\":\"11/08/2015 23:27:42\", \"incorporadora\":\"incorporadora\", \"terminoVistoria\":\"11/08/2015 23:27:42\", \"construtora\":\"construtora\", \"clima\":\"clima\", \"vistoriador\":\"13\"}";
		VistoriaWS ws = new VistoriaWS ();
		String result = ws.save(json);
		System.out.println(result);
	}
	
	public static void testVistoriaWS () {
		VistoriaWS ws = new VistoriaWS ();
		String json = ws.findByVistoriador(18);
		System.out.println(json);
	}
	
	public static void testVistoriadorWS () {
		VistoriadorWS ws = new VistoriadorWS();
		
		Vistoriador vistoriador = new Vistoriador(null, "Vistoriador", "user", "pass");
		String json = vistoriador.getJson();
		ws.save(json);
		
		json = ws.findAll();
		System.out.println(json);
		
	}*/
}
