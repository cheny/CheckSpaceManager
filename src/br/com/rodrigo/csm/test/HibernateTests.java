package br.com.rodrigo.csm.test;

import java.util.Date;

import br.com.rodrigo.csm.dao.VistoriaDAO;
import br.com.rodrigo.csm.dao.VistoriadorDAO;
import br.com.rodrigo.csm.database.DatabaseManager;
import br.com.rodrigo.csm.model.Vistoria;
import br.com.rodrigo.csm.model.Vistoriador;


public class HibernateTests {
	public static void main(String[] args) {
		testCreate();
	}
	
	public static void testCreate(){
		Vistoriador vistoriador = new Vistoriador(null, "Rodrigo", "rodsordi", "1234");
		
		VistoriadorDAO vDAO = new VistoriadorDAO();
		vDAO.setEntityManager(DatabaseManager.getEntityManager());
		vDAO.save(vistoriador);
		
		System.out.println(vistoriador);
		
		
		/*Vistoria vistoria = new Vistoria(null, "Proprietario", "empreendimento",
				"unidade", "bloco", new Date(), 1f, "Padro", new Date(),
				new Date(), new Date(), "Incorporadora", new Date(),
				"Construtora", vistoriador);
		vistoria.setValor(1120.54D);
		vistoria.setClima("Ensolarado");
		
		VistoriaDAO vsDAO = new VistoriaDAO();
		vsDAO.setEntityManager(DatabaseManager.getEntityManager());
		vsDAO.save(vistoria);
		
		System.out.println(vistoria);*/
		
	}
}
