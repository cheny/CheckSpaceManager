package br.com.rodrigo.csm.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.rodrigo.csm.model.JsonBehaviour;
import br.com.rodrigo.csm.model.Vistoriador;


public class JsonTest {
	public static void main(String[] args) {
		testMap ();
	}
	
	public static void testMap() {
		Map<String, Vistoriador> vistMap = new HashMap<String, Vistoriador>();
		for (int i = 0; i < 102; i++) {
			Vistoriador v = new Vistoriador ();
			v.setId(i);
			v.setNome("Nome " + i);
			v.setUsuario("Usuário " + i);
			v.setSenha("Senha " + i);
			vistMap.put (String.valueOf(i), v);
		}
		
		String json = JsonBehaviour.getJsonFromMap(vistMap);
		System.out.println(json);
	}
	
	public static void testLists () {
		List<Vistoriador> vistoriadores = new ArrayList<Vistoriador> ();
		for (int i = 0; i < 102; i++) {
			Vistoriador v = new Vistoriador ();
			v.setId(i);
			v.setNome("Nome " + i);
			v.setUsuario("Usuário " + i);
			v.setSenha("Senha " + i);
			vistoriadores.add (v);
		}
		
		String json = JsonBehaviour.getJsonFromList(vistoriadores);
		
		Map<String, Vistoriador> map = JsonBehaviour.getMapFromJson(Vistoriador.class, json);
		for (Vistoriador vistoriador : map.values()) {
			System.out.println(vistoriador);
		}
		
		/*String idsAsJson = JsonBehaviour.getIdsAsJsonFromMap(map);
		System.out.println(idsAsJson);*/
	}
	
	public static void testVistoria () {
		/*Vistoriador vistoriador = new Vistoriador(1, "Vistoriador", "user", "pass"); 
		Vistoria vistoria = new Vistoria(1, "Proprietario", "Empreendimento", "Unidade", "Bloco", new Date(),1f, "Padrao", new Date(), new Date(), new Date(), "Incorporadora", new Date(), "Construtora", vistoriador);
		System.out.println(vistoriador.getJson());*/
	}
}
