package br.com.rodrigo.csm.test;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.rodrigo.csm.dao.AmbienteDAO;
import br.com.rodrigo.csm.dao.IndicadorDAO;
import br.com.rodrigo.csm.dao.ServicoDAO;
import br.com.rodrigo.csm.dao.VistoriadorDAO;
import br.com.rodrigo.csm.database.DatabaseManager;
import br.com.rodrigo.csm.model.Ambiente;
import br.com.rodrigo.csm.model.Servico;
import br.com.rodrigo.csm.model.Vistoriador;


public class DAOTests {
	public static void main(String[] args) {
		testServico ();
	}
	
	public static void testServico () {
		ServicoDAO dao = new ServicoDAO ();
		dao.setEntityManager(DatabaseManager.getEntityManager());
		
		String result = "";
		
		List<Servico> servicoList = dao.findByVistoria(5);
		for (Servico servico : servicoList)
			System.out.println(servico.getJson());
		
		AmbienteDAO aDAO = new AmbienteDAO ();
		aDAO.setEntityManager(DatabaseManager.getEntityManager());
		Ambiente ambiente = aDAO.findById(30);

		System.out.println(ambiente.getNome());
	}
	
	public static void testNomes () {
		AmbienteDAO aDAO = new AmbienteDAO();
		aDAO.setEntityManager(DatabaseManager.getEntityManager());
		
		String result = "";
		
		List<Ambiente> ambientes = aDAO.findByVistoria(1);
		for (Ambiente a : ambientes)
			result += a.getNome() + ";";
		
		if (result.length() > 0)
			result = result.substring(0, result.length() - 1);
		
		System.out.println(result);
	}
	
	public static void testSave () {
		Vistoriador vistoriador = new Vistoriador(null, "Teste", "Teste", "Teste");
		VistoriadorDAO vDAO = new VistoriadorDAO();
		vDAO.setEntityManager(DatabaseManager.getEntityManager());
		vDAO.save(vistoriador);
		System.out.println(vistoriador.getId());
	}
	
	public static void testConection () {
		VistoriadorDAO vDAO = new VistoriadorDAO();
		vDAO.setEntityManager(DatabaseManager.getEntityManager());
		//vDAO.close();
		EntityManager em = DatabaseManager.getEntityManager();
		
		System.out.println(em.isOpen());
		DatabaseManager.close();
		System.out.println(em.isOpen());
		
		
		System.out.println("Finish");
	}
	
	public static void testImagemVistoria () {
		/*ImagemIndicadorDAO iiDAO = new ImagemIndicadorDAO();
		iiDAO.setEntityManager(DatabaseManager.getEntityManager());
		ImagemIndicador ii = new ImagemIndicador();
		ii.setComentario("Comentario");
		ii.setImagemUrl("sala1.jpg");
		ii.setServico("Ceramica");
		VistoriaDAO vDAO = new VistoriaDAO ();
		vDAO.setEntityManager(DatabaseManager.getEntityManager());
		Vistoria v = vDAO.findById(3);
		System.out.println(v);
		
		ii.setVistoria(v);
		ii.setAmbiente("Sala");
		iiDAO.save(ii);
		
		System.out.println(ii);*/
	}
	
	public static void testIndicador () {
		IndicadorDAO dao = new IndicadorDAO ();
		dao.setEntityManager(DatabaseManager.getEntityManager());
		List<String> result = dao.getNomeAmbientesByVistoria(1);
		for (String string : result) {
			System.out.println(string);
		}
	}
	
}
