package br.com.rodrigo.csm.test;

import java.util.List;

import br.com.rodrigo.csm.dao.AmbienteDAO;
import br.com.rodrigo.csm.dao.ImagemIndicadorDAO;
import br.com.rodrigo.csm.dao.IndicadorDAO;
import br.com.rodrigo.csm.dao.VistoriaDAO;
import br.com.rodrigo.csm.dao.VistoriadorDAO;
import br.com.rodrigo.csm.database.DatabaseManager;
import br.com.rodrigo.csm.model.Ambiente;
import br.com.rodrigo.csm.model.ImagemIndicador;
import br.com.rodrigo.csm.model.Indicador;
import br.com.rodrigo.csm.model.Vistoria;
import br.com.rodrigo.csm.model.Vistoriador;
import br.com.rodrigo.csm.util.WordUtil;

public class POITests {
	public static void main(String[] args) {
		//System.out.println(System.getProperty("user.dir"));
		testWord();
		//savenewfoto();`
		//savenewfotos();
	}

	public static void testWord() {
		VistoriadorDAO vistoriadorDAO = new VistoriadorDAO();
		VistoriaDAO vistoriaDAO = new VistoriaDAO();
		AmbienteDAO ambienteDAO = new AmbienteDAO();
		//ServicoDAO servicoDAO = new ServicoDAO();
		IndicadorDAO indicadorDAO = new IndicadorDAO(); 
		ImagemIndicadorDAO imagemIndicadorDAO = new ImagemIndicadorDAO();
		
		vistoriadorDAO.setEntityManager(DatabaseManager.getEntityManager());
		vistoriaDAO.setEntityManager(DatabaseManager.getEntityManager());
		ambienteDAO.setEntityManager(DatabaseManager.getEntityManager());
		indicadorDAO.setEntityManager(DatabaseManager.getEntityManager());
		imagemIndicadorDAO.setEntityManager(DatabaseManager.getEntityManager());
		
		Vistoriador vistoriador = vistoriadorDAO.login("kaku", "1234");
		List<Vistoria> vistorias = vistoriaDAO.findByVistoriador(vistoriador.getId());
		List<Ambiente> ambientes = ambienteDAO.findByVistoria(vistorias.get(0).getId());
		//List<Servico> servicos = servicoDAO.findByVistoria(vistoriador.getId());
		List<Indicador> indicadores = indicadorDAO.findByVistoria(vistorias.get(0).getId());
		List<ImagemIndicador> imagemIndicadores = imagemIndicadorDAO.findByVistoria(vistorias.get(0).getId());
		
		/*List<String> ambientes = new ArrayList<String>();
		
		for (String ambiente : indicadorDAO.getNomeAmbientesByVistoria(vistorias.get(0).getId()))
			if (!ambientes.contains(ambiente)) 
				ambientes.add (ambiente);*/
		
		WordUtil.getWord(vistorias.get(0), ambientes, indicadores, imagemIndicadores);

		System.out.println("Finish");
	}
	
	public static void savenewfoto () {
		VistoriadorDAO vistoriadorDAO = new VistoriadorDAO();
		VistoriaDAO vistoriaDAO = new VistoriaDAO();
		
		vistoriadorDAO.setEntityManager(DatabaseManager.getEntityManager());
		vistoriaDAO.setEntityManager(DatabaseManager.getEntityManager());
		
		Vistoriador vistoriador = vistoriadorDAO.login("rodsordi", "1234");
		List<Vistoria> vistorias = vistoriaDAO.findByVistoriador(vistoriador.getId());
		
		if (vistorias != null && vistorias.size() > 0) {
			Vistoria vistoria = vistorias.get(0);
			vistoria.setImagemPredioUrl("http://www.robolaranja.com.br/wp-content/uploads/2014/10/Primeira-imagem-do-filme-de-Angry-Birds-%C3%A9-revelada-2.jpg");
			vistoriaDAO.update(vistoria);
		}
	}
	
	/*public static void savenewfotos () {
		VistoriadorDAO vistoriadorDAO = new VistoriadorDAO();
		VistoriaDAO vistoriaDAO = new VistoriaDAO();
		IndicadorDAO indicadorDAO = new IndicadorDAO();
		DimensaoDAO dimensaoDAO = new DimensaoDAO();
		TermoAcusticoDAO termoAcusticoDAO = new TermoAcusticoDAO();
		ImagemIndicadorDAO imagemIndicadorDAO = new ImagemIndicadorDAO();
		ImagemDimensaoDAO imagemDimensaoDAO = new ImagemDimensaoDAO();
		ImagemTermoAcusticoDAO imagemTermoAcusticoDAO = new ImagemTermoAcusticoDAO();
		
		vistoriadorDAO.setEntityManager(DatabaseManager.getEntityManager());
		vistoriaDAO.setEntityManager(DatabaseManager.getEntityManager());
		indicadorDAO.setEntityManager(DatabaseManager.getEntityManager());
		dimensaoDAO.setEntityManager(DatabaseManager.getEntityManager());
		termoAcusticoDAO.setEntityManager(DatabaseManager.getEntityManager());
		imagemIndicadorDAO.setEntityManager(DatabaseManager.getEntityManager());
		imagemDimensaoDAO.setEntityManager(DatabaseManager.getEntityManager());
		imagemTermoAcusticoDAO.setEntityManager(DatabaseManager.getEntityManager());
		
		Vistoriador vistoriador = vistoriadorDAO.login("rodsordi", "1234");
		List<Vistoria> vistorias = vistoriaDAO.findByVistoriador(vistoriador.getId());
		
		if (vistorias != null && vistorias.size() > 0) {
			Vistoria vistoria = vistorias.get(0);
			
			for (Indicador indicador : indicadorDAO.findByVistoria(vistoria.getId())) {
				for (int i = 0; i < 5; i++) {
					String url = "http://www.robolaranja.com.br/wp-content/uploads/2014/10/Primeira-imagem-do-filme-de-Angry-Birds-%C3%A9-revelada-2.jpg";
					ImagemIndicador imagemIndicador = new ImagemIndicador ();
					//imagemIndicador.id = id;
					imagemIndicador.setImagemUrl(url);
					imagemIndicador.setComentario("Comentario");
					imagemIndicador.setIndicador(indicador);
					
					imagemIndicadorDAO.save(imagemIndicador);
				}
			}
				
			for (TermoAcustico termoAcustico : termoAcusticoDAO.findByVistoria(vistoria.getId())) {
				for (int i = 0; i < 5; i++) {
					String url = "http://www.robolaranja.com.br/wp-content/uploads/2014/10/Primeira-imagem-do-filme-de-Angry-Birds-%C3%A9-revelada-2.jpg";
					ImagemTermoAcustico imagemTermoAcustico = new ImagemTermoAcustico ();
					//imagemTermoAcustico.id = id;
					imagemTermoAcustico.setImagemUrl(url);
					imagemTermoAcustico.setComentario("Comentario");
					imagemTermoAcustico.setTermoAcustico(termoAcustico);
					
					imagemTermoAcusticoDAO.save(imagemTermoAcustico);
				}
			}
			
			for (Dimensao dimensao : dimensaoDAO.findByVistoria(vistoria.getId())) {
				for (int i = 0; i < 5; i++) {
					String url = "http://www.robolaranja.com.br/wp-content/uploads/2014/10/Primeira-imagem-do-filme-de-Angry-Birds-%C3%A9-revelada-2.jpg";
					ImagemDimensao imagemDimensao = new ImagemDimensao ();
					//imagemDimensao.id = id;
					imagemDimensao.setImagemUrl(url);
					imagemDimensao.setComentario("Comentario");
					imagemDimensao.setDimensao(dimensao);
					
					imagemDimensaoDAO.save(imagemDimensao);
				}
			}
		}
	}*/
}
