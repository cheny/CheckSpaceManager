package br.com.rodrigo.csm.model;

public final class Servicos {
	/*public static final String CERAMICA = "CERAMICA";
	public static final String SOLEIRAS_E_AFINS = "SOLEIRAS_E_AFINS";
	public static final String PINTURA_E_GESSO = "PINTURA_E_GESSO";
	public static final String PISO_MADEIRA = "PISO_MADEIRA";
	public static final String ESQUADRIAS = "ESQUADRIAS";
	public static final String ELETRICA = "ELETRICA";
	public static final String HIDRAULICA = "HIDRAULICA";*/
	
	public static final String REVESTIMENTO_PISO = "REVESTIMENTO_PISO";
	public static final String REVESTIMENTO_PAREDE = "REVESTIMENTO_PAREDE";
	public static final String SOLEIRAS = "SOLEIRAS";
	public static final String PEITORIS = "PEITORIS";
	public static final String TENTOS_BAGUETES = "TENTOS_BAGUETES";
	public static final String ESQUADRIAS = "ESQUADRIAS";
	public static final String PISO_MADEIRA = "PISO_MADEIRA";
	public static final String PINTURA_TETO = "PINTURA_TETO";
	public static final String PINTURA_PAREDE = "PINTURA_PAREDE";
	public static final String FUNCIONAL_TETO = "FUNCIONAL_TETO";
	public static final String FUNCIONAL_PAREDE = "FUNCIONAL_PAREDE";
	public static final String ELETRICA = "ELETRICA";
	public static final String HIDRAULICA = "HIDRAULICA";
}
