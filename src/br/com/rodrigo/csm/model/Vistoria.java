package br.com.rodrigo.csm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name="vistoria")
public class Vistoria extends JsonBehaviour implements Serializable {
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(nullable=false, length=100)
	private String proprietario;
	
	@Column(nullable=true, length=100)
	private String empreendimento;
	
	@Column(nullable=true)
	private Double valor;
	
	@Column(nullable=true, name="metros_quadrados")
	private Double metrosQuadrados;
	
	@Column(nullable=true, length=255)
	private String endereco;
	
	@Column(nullable=true, length=10)
	private String unidade;
	
	@Column(nullable=true, length=10)
	private String bloco;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=true, name="data_compra")
	private Date dataCompra;
	
	@Column(nullable=true)
	private Double metragem;
	
	@Column(nullable=true, length=45)
	private String padrao;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=true, name="data_vistoria")
	private Date dataVistoria;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true, name="horario_agendado")
	private Date horarioAgendado;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true, name="inicio_vistoria")
	private Date inicioVistoria;
	
	@Column(nullable=true, length=100)
	private String incorporadora;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true, name="termino_vistoria")
	private Date terminoVistoria;
	
	@Column(nullable=true, length=100)
	private String construtora;
	
	@Column(nullable=true, length=100)
	private String clima;
	
	@Column(nullable=true, length=255, name="croqui_esquematico_url")
	private String croquiEsquematicoUrl;
	
	@Column(nullable=true, length=255, name="imagem_predio_url")
	private String imagemPredioUrl;
	
	@ManyToOne()
	@JoinColumn(name="vistoriador_id", nullable=false)
	private Vistoriador vistoriador;
	
	public Vistoria() {}
	
	public Vistoria(String json) {
		setJson(json);
	}

	public Vistoria(Integer id, String proprietario,
			String empreendimento, Double valor, Double metrosQuadrados, String endereco,
			String unidade, String bloco, Date dataCompra, Double metragem,
			String padrao, Date dataVistoria, Date horarioAgendado,
			Date inicioVistoria, String incorporadora, Date terminoVistoria,
			String construtora, String clima, String croquiEsquematicoUrl,
			String imagemPredioUrl, Vistoriador vistoriador) {
		this.id = id;
		this.proprietario = proprietario;
		this.empreendimento = empreendimento;
		this.valor = valor;
		this.metrosQuadrados = metrosQuadrados;
		this.endereco = endereco;
		this.unidade = unidade;
		this.bloco = bloco;
		this.dataCompra = dataCompra;
		this.metragem = metragem;
		this.padrao = padrao;
		this.dataVistoria = dataVistoria;
		this.horarioAgendado = horarioAgendado;
		this.inicioVistoria = inicioVistoria;
		this.incorporadora = incorporadora;
		this.terminoVistoria = terminoVistoria;
		this.construtora = construtora;
		this.clima = clima;
		this.croquiEsquematicoUrl = croquiEsquematicoUrl;
		this.imagemPredioUrl = imagemPredioUrl;
		this.vistoriador = vistoriador;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public String getEmpreendimento() {
		return empreendimento;
	}

	public void setEmpreendimento(String empreendimento) {
		this.empreendimento = empreendimento;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getBloco() {
		return bloco;
	}

	public void setBloco(String bloco) {
		this.bloco = bloco;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Double getMetragem() {
		return metragem;
	}

	public void setMetragem(Double metragem) {
		this.metragem = metragem;
	}

	public String getPadrao() {
		return padrao;
	}

	public void setPadrao(String padrao) {
		this.padrao = padrao;
	}

	public Date getDataVistoria() {
		return dataVistoria;
	}

	public void setDataVistoria(Date dataVistoria) {
		this.dataVistoria = dataVistoria;
	}

	public Date getHorarioAgendado() {
		return horarioAgendado;
	}

	public void setHorarioAgendado(Date horarioAgendado) {
		this.horarioAgendado = horarioAgendado;
	}

	public Date getInicioVistoria() {
		return inicioVistoria;
	}

	public void setInicioVistoria(Date inicioVistoria) {
		this.inicioVistoria = inicioVistoria;
	}

	public String getIncorporadora() {
		return incorporadora;
	}

	public void setIncorporadora(String incorporadora) {
		this.incorporadora = incorporadora;
	}

	public Date getTerminoVistoria() {
		return terminoVistoria;
	}

	public void setTerminoVistoria(Date terminoVistoria) {
		this.terminoVistoria = terminoVistoria;
	}

	public String getConstrutora() {
		return construtora;
	}

	public void setConstrutora(String construtora) {
		this.construtora = construtora;
	}

	public String getClima() {
		return clima;
	}

	public void setClima(String clima) {
		this.clima = clima;
	}

	public String getCroquiEsquematicoUrl() {
		return croquiEsquematicoUrl;
	}

	public void setCroquiEsquematicoUrl(String croquiEsquematicoUrl) {
		this.croquiEsquematicoUrl = croquiEsquematicoUrl;
	}

	public String getImagemPredioUrl() {
		return imagemPredioUrl;
	}

	public void setImagemPredioUrl(String imagemPredioUrl) {
		this.imagemPredioUrl = imagemPredioUrl;
	}

	public Vistoriador getVistoriador() {
		return vistoriador;
	}

	public void setVistoriador(Vistoriador vistoriador) {
		this.vistoriador = vistoriador;
	}

	public Double getMetrosQuadrados() {
		return metrosQuadrados;
	}

	public void setMetrosQuadrados(Double metrosQuadrados) {
		this.metrosQuadrados = metrosQuadrados;
	}
	
}
