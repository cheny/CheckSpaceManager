package br.com.rodrigo.csm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="imagem_indicador")
public class ImagemIndicador extends JsonBehaviour implements Serializable {
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(nullable=true, length=255, name="imagem_url")
	private String imagemUrl;
	
	@Column(nullable=true, length=100)
	private String tag;
	
	@Column(nullable=true, length=255)
	private String comentario;
	
	@ManyToOne()
	@JoinColumn(name="indicador_id", nullable=false)
	private Indicador indicador;
	
	public ImagemIndicador() {}
	
	public ImagemIndicador(String json) {
		setJson(json);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getImagemUrl() {
		return imagemUrl;
	}

	public void setImagemUrl(String imagemUrl) {
		this.imagemUrl = imagemUrl;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Indicador getIndicador() {
		return indicador;
	}

	public void setIndicador(Indicador indicador) {
		this.indicador = indicador;
	}
	
	
}
