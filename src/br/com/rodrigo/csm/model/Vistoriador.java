package br.com.rodrigo.csm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="vistoriador")
public class Vistoriador extends JsonBehaviour implements Serializable {
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(nullable=false, length=100)
	private String nome;
	
	@Column(nullable=false, length=10, unique=true)
	private String usuario;
	
	@Column(nullable=false, length=10)
	private String senha;
	
	public Vistoriador() {}
	
	public Vistoriador(String json) {
		setJson(json);
	}
	
	public Vistoriador(Integer id, String nome, String usuario, String senha) {
		this.id = id;
		this.nome = nome;
		this.usuario = usuario;
		this.senha = senha;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public String toString() {
		return "Vistoriador [id=" + id + ", nome=" + nome + ", usuario="
				+ usuario + ", senha=" + senha + "]";
	}
	
}
