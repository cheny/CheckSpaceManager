package br.com.rodrigo.csm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="ambiente")
public class Ambiente extends JsonBehaviour implements Serializable {
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(nullable=true, name="pe_direito")
	private Double peDireito;
	
	@Column(nullable=true)
	private Double area;
	
	@Column(nullable=false, length=100)
	private String nome;
	
	@Column(nullable=true, name="acustico_aberto")
	private Double acusticoAberto;
	
	@Column(nullable=true, name="acustico_fechado")
	private Double acusticoFechado;
	
	@Column(nullable=true, name="termo_aberto")
	private Double termoAberto;
	
	@Column(nullable=true, name="termo_fechado")
	private Double termoFechado;
	
	@Column(nullable=true, name="acustico_aberto_com_res")
	private String acusticoAbertoComRes;
	
	@Column(nullable=true, name="acustico_fechado_com_res")
	private String acusticoFechadoComRes;
	
	@Column(nullable=true, name="termo_aberto_com_res")
	private String termoAbertoComRes;
	
	@Column(nullable=true, name="termo_fechado_com_res")
	private String termoFechadoComRes;
	
	@ManyToOne()
	@JoinColumn(name="vistoria_id", nullable=false)
	private Vistoria vistoria;
	
	public Ambiente () {}
	
	public Ambiente (String json) {
		setJson(json);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Vistoria getVistoria() {
		return vistoria;
	}

	public void setVistoria(Vistoria vistoria) {
		this.vistoria = vistoria;
	}

	public Double getPeDireito() {
		return peDireito;
	}

	public void setPeDireito(Double peDireito) {
		this.peDireito = peDireito;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public Double getAcusticoAberto() {
		return acusticoAberto;
	}

	public void setAcusticoAberto(Double acusticoAberto) {
		this.acusticoAberto = acusticoAberto;
	}

	public Double getAcusticoFechado() {
		return acusticoFechado;
	}

	public void setAcusticoFechado(Double acusticoFechado) {
		this.acusticoFechado = acusticoFechado;
	}

	public Double getTermoAberto() {
		return termoAberto;
	}

	public void setTermoAberto(Double termoAberto) {
		this.termoAberto = termoAberto;
	}

	public Double getTermoFechado() {
		return termoFechado;
	}

	public void setTermoFechado(Double termoFechado) {
		this.termoFechado = termoFechado;
	}

	public String getAcusticoAbertoComRes() {
		return acusticoAbertoComRes;
	}

	public void setAcusticoAbertoComRes(String acusticoAbertoComRes) {
		this.acusticoAbertoComRes = acusticoAbertoComRes;
	}

	public String getAcusticoFechadoComRes() {
		return acusticoFechadoComRes;
	}

	public void setAcusticoFechadoComRes(String acusticoFechadoComRes) {
		this.acusticoFechadoComRes = acusticoFechadoComRes;
	}

	public String getTermoAbertoComRes() {
		return termoAbertoComRes;
	}

	public void setTermoAbertoComRes(String termoAbertoComRes) {
		this.termoAbertoComRes = termoAbertoComRes;
	}

	public String getTermoFechadoComRes() {
		return termoFechadoComRes;
	}

	public void setTermoFechadoComRes(String termoFechadoComRes) {
		this.termoFechadoComRes = termoFechadoComRes;
	}
	
	
}
