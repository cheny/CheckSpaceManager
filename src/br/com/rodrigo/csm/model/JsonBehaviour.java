package br.com.rodrigo.csm.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class JsonBehaviour implements Serializable {
	private Class<?> entityClass;

	public String getJson() {
		JSONObject jObj = new JSONObject();

		for (Field field : getEntityClass().getDeclaredFields()) {
			String name = field.getName();
			field.setAccessible(true);
			try {
				if (field.getName().equals("serialVersionUID")) {
					continue;
				} else if (field.get(this) instanceof Date) {
					Date date = (Date) field.get(this);
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Object value = df.format(date);
					jObj.put(name, value);
				} else if (field.get(this) instanceof JsonBehaviour) {
					JsonBehaviour jbo = (JsonBehaviour) field.get(this);
					Object value = getId(jbo);
					jObj.put(name, value);
				} else {
					Object value = field.get(this);
					jObj.put(name, value);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return jObj.toString();
	}

	protected void setJson(String json) {
		JSONObject jObj = new JSONObject(json);
		
		for (Field field : getEntityClass().getDeclaredFields()) {
			String name = field.getName();
			field.setAccessible(true);
			if (jObj.has(name)) {
				try {
					if (field.getName().equals("serialVersionUID")) {
						continue;
					} else if (field.getType().equals(Date.class)) {
						String value = jObj.getString(name);
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						Date date = df.parse(value);
						field.set(this, date);
					} else if (field.getType().getSuperclass().equals(JsonBehaviour.class)) {
						JsonBehaviour jbo = (JsonBehaviour)field.getType().newInstance();
						Object value = jObj.get(name);
						setId(jbo, value);
						field.set(this, jbo);
					} else {
						if (field.getType().equals(Integer.class)) {
							field.set(this, jObj.getInt(name));
						} else if (field.getType().equals(String.class)) {
							field.set(this, jObj.getString(name));
						} else if (field.getType().equals(Character.class)) {
							field.set(this, jObj.getString(name).charAt(0));
						}else if (field.getType().equals(Boolean.class)) {
							field.set(this, jObj.getBoolean(name));
						}else if (field.getType().equals(Double.class)) {
							field.set(this, jObj.getDouble(name));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	// Detecta se a referencia possuí a annotation @Id e retorna seu valor
	private Object getId(JsonBehaviour jbo) {
		Object result = null;
		for (Field field : jbo.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			for (Annotation annotation : field.getDeclaredAnnotations()) {
				if (annotation.toString().equals("@javax.persistence.Id()")) {
					try {
						result = field.get(jbo);
						break;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return result;
	}

	// Detecta o id do objeto e preenche com o valor indicado
	private void setId(JsonBehaviour jbo, Object id) {
		for (Field field : jbo.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			for (Annotation annotation : field.getDeclaredAnnotations()) {
				if (annotation.toString().equals("@javax.persistence.Id()")) {
					try {
						if (field.getType().equals(Integer.class)) {
							System.out.println(id);
							field.set(jbo, Integer.parseInt((String)id));
							break;
						} else if (field.getType().equals(String.class)) {
							field.set(jbo, (String) id);
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public static <T extends JsonBehaviour> String getJsonFromList (List<T> modelList) {
		String result = null;
		if (modelList != null && modelList.size() > 0) {
			JSONObject jsonList = new JSONObject();
			
			@SuppressWarnings("unchecked")
			Class<T> clazz = (Class<T>) modelList.get(0).getClass();
			
			String max = String.valueOf (modelList.size() -1);
			for (int i = 0; i < modelList.size(); i++) {
				if (modelList.get(i) == null)
					continue;
				
				String index = String.valueOf(i);
				while (index.length() < max.length())
					index = "0" + index;
					
				String key = clazz.getSimpleName() + "-" + index;
				String json = modelList.get(i).getJson();
				JSONObject jsonObj = new JSONObject(json);
				jsonList.put(key, jsonObj);
			}
			
			result = jsonList.toString();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends JsonBehaviour> String getJsonFromMap (Map<String, T> entityMap) {
		String result = null;
		if (entityMap != null && entityMap.size() > 0) {
			JSONObject jsonList = new JSONObject();
			
			Class<T> clazz = null; 
			
			String max = String.valueOf (entityMap.size() -1);
			int i = 0;
			for (Map.Entry<String, T> entry : entityMap.entrySet()) {
				if (entry.getValue() == null)
					continue;
				
				if (clazz == null)
					clazz = (Class<T>) entry.getValue().getClass();
				
				String index = String.valueOf(i);
				while (index.length() < max.length())
					index = "0" + index;
					
				String key = clazz.getSimpleName() + "-" + index;
				String json = entry.getValue().getJson();
				JSONObject jsonObj = new JSONObject(json);
				jsonList.put(key, jsonObj);
				i++;
			}
			
			result = jsonList.toString();
		}
		return result;
	}
	
	/*public static <T extends JsonBehaviour> List<T> getListFromJson (Class<T> clazz, String jsonList) {
		List<T> result = null;
		if (jsonList != null && !jsonList.equals("")) {
			typeValidate(clazz, jsonList);
			JSONObject jObj = new JSONObject(jsonList);
			
			String[] names = JSONObject.getNames(jObj);
			if (names != null && names.length > 0) {
				Arrays.sort(names);
				result = new ArrayList<T>();
			}
			
			for (String name : names) {
				Object json = jObj.get(name);
				try {
					T entity = clazz.newInstance();
					entity.setJson(json.toString());
					result.add(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}*/
	
	public static <T extends JsonBehaviour> Map<String, T> getMapFromJson (Class<T> clazz, String jsonList) {
		Map<String, T> result = null;
		if (jsonList != null && !jsonList.equals("")) {
			typeValidate(clazz, jsonList);
			
			JSONObject jObj = new JSONObject(jsonList);
			
			String[] names = JSONObject.getNames(jObj);
			if (names != null && names.length > 0)
				result = new HashMap<String, T>();
			
			for (String name : names) {
				Object json = jObj.get(name);
				try {
					T entity = clazz.newInstance();
					entity.setJson(json.toString());
					result.put(name, entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}
	
	/*public static <T extends JsonBehaviour> String getIdsAsJsonFromMap(Map<String, T> mapOfEntities) {
		String result = null;
		
		if (mapOfEntities != null && mapOfEntities.size() > 0) {
			JSONObject jObj = new JSONObject();
			
			for (Map.Entry<String, T> entry : mapOfEntities.entrySet()) {
				//Finding Id attribute
				T entity = entry.getValue();
				for (Field field : entity.getClass().getDeclaredFields()) {
					field.setAccessible(true);
					for (Annotation annotation : field.getDeclaredAnnotations()) {
						if (annotation.toString().equals("@javax.persistence.Id()")) {
							try {
								jObj.put(entry.getKey(), field.get(entity));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
			
			result = jObj.toString();
		}
		
		return result;
	}*/
	
	public Object getId() {
		Object result = null;
		for (Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			for (Annotation annotation : field.getDeclaredAnnotations()) {
				if (annotation.toString().equals("@javax.persistence.Id()")) {
					try {
						result = field.get(this);
						break;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return result;
	}
	
	public void setId(Object id) {
		for (Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			for (Annotation annotation : field.getDeclaredAnnotations()) {
				if (annotation.toString().equals("@javax.persistence.Id()")) {
					try {
						if (id == null) {
							field.set(this, null);
							break;
							
						} else if (field.getType().equals(Integer.class)) {
							field.set(this, Integer.parseInt((String)id));
							break;
							
						} else if (field.getType().equals(String.class)) {
							field.set(this, (String) id);
							break;
							
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private Class<?> getEntityClass () {
		if (entityClass == null) {
			entityClass = (Class<?>) getClass();
		}
		return entityClass;
	}
	
	private static <T extends JsonBehaviour> void typeValidate(Class<T> clazz, String jsonList) {
		JSONObject jObj = new JSONObject(jsonList);
		
		String[] names = JSONObject.getNames(jObj);
		if (names.length > 0) {
			String name = names[0];
			String className = null;
			
			if (name.contains("-"))
				className = name.split("-")[0];
			
			if (!clazz.getSimpleName().equals(className))
				throw new RuntimeException("Json não pertence à esta classe!");
		}
	}
}