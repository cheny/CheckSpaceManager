package br.com.rodrigo.csm.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.rodrigo.csm.dao.AmbienteDAO;
import br.com.rodrigo.csm.dao.ImagemIndicadorDAO;
import br.com.rodrigo.csm.dao.IndicadorDAO;
import br.com.rodrigo.csm.dao.VistoriaDAO;
import br.com.rodrigo.csm.database.DatabaseManager;
import br.com.rodrigo.csm.model.Ambiente;
import br.com.rodrigo.csm.model.ImagemIndicador;
import br.com.rodrigo.csm.model.Indicador;
import br.com.rodrigo.csm.model.Vistoria;
import br.com.rodrigo.csm.model.Vistoriador;
import br.com.rodrigo.csm.util.WordUtil;

@ManagedBean
public class VistoriasBean {
	private VistoriaDAO vistoriaDAO;
	private ImagemIndicadorDAO imagemIndicadorDAO;
	private IndicadorDAO indicadorDAO;
	private AmbienteDAO ambienteDAO;
	
	private List<Vistoria> vistorias;
	
	private Map<String, Object> sessionMap;

	@PostConstruct
	public void init() {
		try {
			sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			Vistoriador vistoriador = (Vistoriador) sessionMap.get("vistoriador");
		
			vistoriaDAO = new VistoriaDAO();
			vistoriaDAO.setEntityManager(DatabaseManager.getEntityManager());
		
			if (vistoriador != null && vistoriador.getUsuario().equals("admin")) {
				vistorias = vistoriaDAO.findAll();
			} else if (vistoriador != null) {
				vistorias = vistoriaDAO.findByVistoriador(vistoriador.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
			DatabaseManager.closeFactory();
		} finally {
			DatabaseManager.close();			
		}
	}

	public StreamedContent download(Vistoria vistoria) {
		try {
			ambienteDAO = new AmbienteDAO();
			indicadorDAO = new IndicadorDAO();
			imagemIndicadorDAO = new ImagemIndicadorDAO();
			
			ambienteDAO.setEntityManager(DatabaseManager.getEntityManager());
			indicadorDAO.setEntityManager(DatabaseManager.getEntityManager());
			imagemIndicadorDAO.setEntityManager(DatabaseManager.getEntityManager());
			
			List<Ambiente> ambientes = ambienteDAO.findByVistoria(vistoria.getId());
			List<Indicador> indicadores = indicadorDAO.findByVistoria(vistoria.getId());
			List<ImagemIndicador> imagemIndicadores = imagemIndicadorDAO.findByVistoria(vistoria.getId());
			
			File file = WordUtil.getWord(vistoria, ambientes, indicadores, imagemIndicadores);
		
			InputStream stream = new FileInputStream (file);
			return new DefaultStreamedContent(stream, "word/docx", "checkspace.docx");
			
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
			DatabaseManager.closeFactory();
			return null;
		} finally {
			DatabaseManager.close();
		}
	}

	public String logoff() {
		sessionMap.remove("vistoriador");
		return "home";
	}

	public List<Vistoria> getVistorias() {
		return vistorias;
	}

	public void setVistorias(List<Vistoria> vistorias) {
		this.vistorias = vistorias;
	}
}
