package br.com.rodrigo.csm.bean;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import br.com.rodrigo.csm.dao.VistoriadorDAO;
import br.com.rodrigo.csm.database.DatabaseManager;
import br.com.rodrigo.csm.model.Vistoriador;

@ManagedBean
public class HomeBean {
	private VistoriadorDAO vistoriadorDAO;
	
	private String userName;
	private String password;
	
	private Map<String, Object> sessionMap;
	
	@PostConstruct
	public void init() {
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	}
	
	public String login() {
		try {
			vistoriadorDAO = new VistoriadorDAO ();
			vistoriadorDAO.setEntityManager(DatabaseManager.getEntityManager());
			Vistoriador vistoriador = vistoriadorDAO.login (userName, password);
			
			if (vistoriador != null) {
				sessionMap.put("vistoriador", vistoriador);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Login efetuado com sucesso!"));
				return "vistorias";
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário ou senha incorretos!.", null));
				return "home";
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro no servidor!.", null));
			DatabaseManager.closeFactory();
			return "home";
		} finally {
			DatabaseManager.close();
		}
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
