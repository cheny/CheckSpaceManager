package br.com.rodrigo.csm.database;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class DatabaseManager {
	private static EntityManagerFactory factory;
	private static EntityManager manager;
	
	public static EntityManagerFactory getFactory () {
		if (factory == null || !factory.isOpen()) 
			factory = Persistence.createEntityManagerFactory("kinghost_checkspace_db");
		return factory;
	}
	
	public static void closeFactory () {
		if (factory != null && factory.isOpen())
			factory.close();
	}
	
	
	public static EntityManager getEntityManagerNew () {
		return getFactory().createEntityManager();
	}
	
	public static EntityManager getEntityManager () {
		if (manager == null || !manager.isOpen() || factory == null || !factory.isOpen())
			manager = getFactory().createEntityManager();
		return manager;
	}
	
	public static void close () {
		if (manager != null && manager.isOpen())
			manager.close();
	}
	
}
