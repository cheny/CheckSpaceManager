package br.com.rodrigo.csm.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.codec.net.URLCodec;

import br.com.rodrigo.csm.dao.AmbienteDAO;
import br.com.rodrigo.csm.dao.ImagemIndicadorDAO;
import br.com.rodrigo.csm.dao.IndicadorDAO;
import br.com.rodrigo.csm.dao.ServicoDAO;
import br.com.rodrigo.csm.dao.VistoriaDAO;
import br.com.rodrigo.csm.dao.VistoriadorDAO;
import br.com.rodrigo.csm.database.DatabaseManager;
import br.com.rodrigo.csm.model.Ambiente;
import br.com.rodrigo.csm.model.ImagemIndicador;
import br.com.rodrigo.csm.model.Indicador;
import br.com.rodrigo.csm.model.JsonBehaviour;
import br.com.rodrigo.csm.model.Servico;
import br.com.rodrigo.csm.model.Vistoria;
import br.com.rodrigo.csm.model.Vistoriador;

public class Server {
	
	private static final String PHOTO_PATH = "/home/checkspace/images";
	//private static final String PHOTO_PATH = "d:\\home\\site\\wwwroot\\webapps\\images";
	//private static final String PHOTO_PATH = "/users/rodrigosordi/desktop/images";
	
	private EntityManager manager;
	
	public Server () {
		this.manager = DatabaseManager.getEntityManagerNew();
	}
	
	//Vistoriador
	
	public String saveOrUpdateVistoriador (String json) {
		String result = null;
		VistoriadorDAO dao = null;
		try {
			dao = new VistoriadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Vistoriador vistoriador = new Vistoriador (json);
			if (vistoriador.getId() == null || vistoriador.getId() == 0) {
				vistoriador.setId(null);
				dao.save(vistoriador);
			} else
				dao.update(vistoriador);
			result = vistoriador.getId().toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String saveVistoriadorList (String json) {
		String result = null;
		VistoriadorDAO dao = null;
		try {
			dao = new VistoriadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Map<String, Vistoriador> vistoriadorMap = JsonBehaviour.getMapFromJson(Vistoriador.class, json);
			
			if (vistoriadorMap != null && vistoriadorMap.size() > 0) {
				dao.saveOrUpdateMap(vistoriadorMap);
				result = JsonBehaviour.getJsonFromMap(vistoriadorMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String login (String user_pass) {
		String result = null;
		
		if (user_pass == null || !user_pass.contains("_"))
			return WebServiceExeptionManager.getExceptionMessage(new Exception ("Senha ou password incorretos!"));
			
		String usuario = user_pass.split("_") [0];
		String senha = user_pass.split("_") [1];
		
		VistoriadorDAO dao = null;
		try {
			dao = new VistoriadorDAO();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Vistoriador vistoriador = dao.login(usuario, senha);
			if (vistoriador != null) 
				result = vistoriador.getJson();
			
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findAllVistoriador () {
		String result = null;
		
		VistoriadorDAO dao = null;
		try {
			dao = new VistoriadorDAO();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Vistoriador> vistoriadores = dao.findAll();
			result = JsonBehaviour.getJsonFromList(vistoriadores);
			
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	//Vistoria
	
	public String saveOrUpdateVistoria (String json) {
		String result = null;
		
		VistoriaDAO dao = null;
		try {
			dao = new VistoriaDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Vistoria vistoria = new Vistoria (json);
			if (vistoria.getId() == null || vistoria.getId() == 0) {
				vistoria.setId(null);
				dao.save(vistoria);
			} else
				dao.update(vistoria);
			result = vistoria.getId().toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String saveVistoriaList (String json) {
		String result = null;
		VistoriaDAO dao = null;
		try {
			dao = new VistoriaDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Map<String, Vistoria> vistoriaMap = JsonBehaviour.getMapFromJson(Vistoria.class, json);
			
			if (vistoriaMap != null && vistoriaMap.size() > 0) {
				dao.saveOrUpdateMap(vistoriaMap);
				result = JsonBehaviour.getJsonFromMap(vistoriaMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findVistoriaByVistoriador (int vistoriadorId) {
		String result = null;
		VistoriaDAO dao = null;
		try {
			dao = new VistoriaDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Vistoria> vistorias = dao.findByVistoriador(vistoriadorId);
			String json = JsonBehaviour.getJsonFromList(vistorias);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	//Ambiente
	public String saveOrUpdateAmbiente (String json) {
		String result = null;
		AmbienteDAO dao = null;
		try {
			dao = new AmbienteDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Ambiente ambiente = new Ambiente (json);
			if (ambiente.getId() == null || ambiente.getId() == 0) {
				ambiente.setId(null);
				dao.save(ambiente);
			} else
				dao.update(ambiente);
			result = ambiente.getId().toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String saveAmbienteList (String json) {
		String result = null;
		AmbienteDAO dao = null;
		try {
			dao = new AmbienteDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Map<String, Ambiente> ambienteMap = JsonBehaviour.getMapFromJson(Ambiente.class, json);
			
			if (ambienteMap != null && ambienteMap.size() > 0) {
				dao.saveOrUpdateMap(ambienteMap);
				result = JsonBehaviour.getJsonFromMap(ambienteMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findAmbienteByVistoria (int vistoriaId) {
		String result = null;
		AmbienteDAO dao = null;
		try {
			dao = new AmbienteDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Ambiente> ambientes = dao.findByVistoria(vistoriaId);
			String json = JsonBehaviour.getJsonFromList(ambientes);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String getNomeAmbientesByVistoria (int vistoriaId) {
		String result = "";
		AmbienteDAO dao = null;
		try {
			dao = new AmbienteDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Ambiente> ambientes = dao.findByVistoria(vistoriaId);
			for (Ambiente a : ambientes)
				result += a.getNome() + ";";
			
			if (result.length() > 0)
				result = result.substring(0, result.length() - 1);
			
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	//Servico
	
	public String saveOrUpdateServico (String json) {
		String result = null;
		ServicoDAO dao = null;
		try {
			dao = new ServicoDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Servico servico = new Servico (json);
			if (servico.getId() == null || servico.getId() == 0) {
				servico.setId(null);
				dao.save(servico);
			} else
				dao.update(servico);
			result = servico.getId().toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String saveServicoList (String json) {
		String result = null;
		ServicoDAO dao = null;
		try {
			dao = new ServicoDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Map<String, Servico> servicoMap = JsonBehaviour.getMapFromJson(Servico.class, json);
			
			if (servicoMap != null && servicoMap.size() > 0) {
				dao.saveOrUpdateMap(servicoMap);
				result = JsonBehaviour.getJsonFromMap(servicoMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findServicoByVistoria (int vistoriaId) {
		String result = null;
		ServicoDAO dao = null;
		try {
			dao = new ServicoDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Servico> servicos = dao.findByVistoria(vistoriaId);
			String json = JsonBehaviour.getJsonFromList(servicos);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findServicoByAmbiente (int ambienteId) {
		String result = null;
		ServicoDAO dao = null;
		try {
			dao = new ServicoDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Servico> servicos = dao.findByAmbiente(ambienteId);
			String json = JsonBehaviour.getJsonFromList(servicos);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	//Indicador
	
	public String saveOrUpdateIndicador (String json) {
		String result = null;
		IndicadorDAO dao = null;
		try {
			dao = new IndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Indicador indicador = new Indicador (json);
			if (indicador.getId() == null || indicador.getId() == 0) {
				indicador.setId(null);
				dao.save(indicador);
			} else
				dao.update(indicador);
			result = indicador.getId().toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String saveIndicadorList (String json) {
		String result = null;
		IndicadorDAO dao = null;
		try {
			dao = new IndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Map<String, Indicador> indicadorMap = JsonBehaviour.getMapFromJson(Indicador.class, json);
			
			if (indicadorMap != null && indicadorMap.size() > 0) {
				dao.saveOrUpdateMap(indicadorMap);
				result = JsonBehaviour.getJsonFromMap(indicadorMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findIndicadorByVistoriaAndAmbienteAndServico (String vistoriaId_ambiente_servico) {
		String result = null;
		
		if (!vistoriaId_ambiente_servico.contains(";"))
			return "Exception - Erro no formato do parametro, deve conter ponto e vírgulas!";
		
		if (vistoriaId_ambiente_servico.split(";").length != 3)
			return "Exception - Erro no formato do parametro, deve conter 2 ponto e vírgulas!";
		
		int vistoriaId = Integer.parseInt(vistoriaId_ambiente_servico.split(";")[0]);
		String ambiente = vistoriaId_ambiente_servico.split(";")[1];
		String servico = vistoriaId_ambiente_servico.split(";")[2];
			
		IndicadorDAO dao = null;
		try {
			dao = new IndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Indicador> indicardors = dao.findByVistoriaAndAmbienteAndServico(vistoriaId, ambiente, servico);
			String json = JsonBehaviour.getJsonFromList(indicardors);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findIndicadorByVistoriaAndAmbienteAndServicoAndNome (String vistoriaId_ambiente_servico_nome) {
		String result = null;
		
		if (!vistoriaId_ambiente_servico_nome.contains(";"))
			return "Exception - Erro no formato do parametro, deve conter ponto e vírgulas!";
		
		if (vistoriaId_ambiente_servico_nome.split(";").length != 4)
			return "Exception - Erro no formato do parametro, deve conter 3 ponto e vírgulas!";
		
		int vistoriaId = Integer.parseInt(vistoriaId_ambiente_servico_nome.split(";")[0]);
		String ambiente = vistoriaId_ambiente_servico_nome.split(";")[1];
		String servico = vistoriaId_ambiente_servico_nome.split(";")[2];
		String nome = vistoriaId_ambiente_servico_nome.split(";")[3];
			
		IndicadorDAO dao = null;
		try {
			dao = new IndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Indicador> indicardors = dao.findByVistoriaAndAmbienteAndServicoAndName(vistoriaId, ambiente, servico, nome);
			String json = JsonBehaviour.getJsonFromList(indicardors);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findIndicadorByVistoriador (int vistoriadorId) {
		String result = null;
		IndicadorDAO dao = null;
		try {
			dao = new IndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Indicador> indicardors = dao.findByVistoriador(vistoriadorId);
			String json = JsonBehaviour.getJsonFromList(indicardors);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findIndicadorByVistoria (int vistoriaId) {
		String result = null;
		IndicadorDAO dao = null;
		try {
			dao = new IndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<Indicador> indicardors = dao.findByVistoria(vistoriaId);
			String json = JsonBehaviour.getJsonFromList(indicardors);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	//ImagemIndicador
	
	public String saveOrUpdateImagemIndicador (String json) {
		String result = null;
		ImagemIndicadorDAO dao = null;
		try {
			dao = new ImagemIndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			ImagemIndicador imagemIndicador = new ImagemIndicador (json);
			if (imagemIndicador.getId() == null || imagemIndicador.getId() == 0) {
				imagemIndicador.setId(null);
				dao.save(imagemIndicador);
			} else
				dao.update(imagemIndicador);
			result = imagemIndicador.getId().toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String saveImagemIndicadorList (String json) {
		String result = null;
		ImagemIndicadorDAO dao = null;
		try {
			dao = new ImagemIndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			Map<String, ImagemIndicador> imagemIndicadorMap = JsonBehaviour.getMapFromJson(ImagemIndicador.class, json);
			
			if (imagemIndicadorMap != null && imagemIndicadorMap.size() > 0) {
				dao.saveOrUpdateMap(imagemIndicadorMap);
				result = JsonBehaviour.getJsonFromMap(imagemIndicadorMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findImagemIndicadorByVistoriaAndAmbienteAndServico (String vistoriaId_ambiente_servico) {
		String result = null;
		if (!vistoriaId_ambiente_servico.contains(";"))
			return "Exception - Erro no formato do parametro, deve conter ponto e vírgulas!";
		
		if (vistoriaId_ambiente_servico.split(";").length != 3)
			return "Exception - Erro no formato do parametro, deve conter 2 ponto e vírgulas!";
		
		int vistoriaId = Integer.parseInt(vistoriaId_ambiente_servico.split(";")[0]);
		String ambiente = vistoriaId_ambiente_servico.split(";")[1];
		String servico = vistoriaId_ambiente_servico.split(";")[2];
		
		ImagemIndicadorDAO dao = null;
		try {
			dao = new ImagemIndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<ImagemIndicador> imagemIndicardors = dao.findByVistoriaAndAmbienteAndServico(vistoriaId, ambiente, servico);
			String json = JsonBehaviour.getJsonFromList(imagemIndicardors);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findImagemIndicadorByVistoriador (int vistoriadorId) {
		String result = null;
		ImagemIndicadorDAO dao = null;
		try {
			dao = new ImagemIndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<ImagemIndicador> imagemIndicardors = dao.findByVistoriador(vistoriadorId);
			String json = JsonBehaviour.getJsonFromList(imagemIndicardors);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String findImagemIndicadorByVistoria (int vistoriaId) {
		String result = null;
		ImagemIndicadorDAO dao = null;
		try {
			dao = new ImagemIndicadorDAO ();
			if (manager == null || !manager.isOpen())
				manager = DatabaseManager.getEntityManagerNew();
			dao.setEntityManager(manager);
			
			List<ImagemIndicador> imagemIndicardors = dao.findByVistoria(vistoriaId);
			String json = JsonBehaviour.getJsonFromList(imagemIndicardors);
			result = json;
		} catch (Exception e) {
			e.printStackTrace();
			result = WebServiceExeptionManager.getExceptionMessage(e);
			DatabaseManager.closeFactory();
		}
		return result;
	}
	
	public String saveImage (String folder_name_encoded) {
		String url = null;
		try {
			if (folder_name_encoded != null && folder_name_encoded.contains(";")) {
				String folderName = folder_name_encoded.split(";")[0];
				String name = folder_name_encoded.split(";")[1];
				String encodedPhoto = folder_name_encoded.split(";")[2];
				
				if (!name.contains(".jpg") && !name.contains(".bmp") && !name.contains(".gif") && !name.contains(".jpeg"))
					name += ".jpg";
				
				name = name.replace(' ', '_');
				
				//Create images folder
				File dir = new File (PHOTO_PATH);
				if (!dir.exists()) 
					dir.mkdir();
				
				//Create vistoria image folder
				dir = new File (PHOTO_PATH + File.separator + folderName);
				if (!dir.exists()) 
					dir.mkdir();
				
				url = PHOTO_PATH + File.separator + folderName + File.separator + name;
				File file = new File(url);
				
				URLCodec codec = new URLCodec();
				byte[] data = codec.decode(encodedPhoto.getBytes());
				
				FileOutputStream out = new FileOutputStream(file);
				out.write(data);
				out.close();
			} else {
				url = "Exception\n"
						+ "O parametro deve conter 2 pontos e virgulas!";
			}
		}  catch (Exception e) {
			e.printStackTrace();
			url = WebServiceExeptionManager.getExceptionMessage(e);
		}
		return url;
	}
	
	public String getImage (String url) {
		String result = null;
		try {
			if (url != null && !url.equals("")) {
				File file = new File(url);
				if (file != null && file.exists()) {
					URLCodec codec = new URLCodec();
					Path path = Paths.get (file.getPath());
					byte[] bytes = Files.readAllBytes(path);
					bytes = codec.encode(bytes);
					result = new String (bytes);
				} else {
					result = "Exception\n"
							+ "Imagem não encontrada no servidor!";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			url = WebServiceExeptionManager.getExceptionMessage(e);
		}
		return result;
	}
	
	public void close(int priority) {
		DatabaseManager.close();
	}
	
}
